package lucian.leagueassistant.activity;

/**
 * Created by Lucian on 14-Jun-17.
 */

public class SummonerListItem {
    private byte[] icon;
    private String name;
    private String region;
    private boolean checked;

    public SummonerListItem(String name,byte[] icon) {
        super();
        setChecked(false);
        setIcon(icon);
        setName(name);
    }

    public byte[] getIcon() {
        return icon;
    }

    public void setIcon(byte[] icon) {
        this.icon = icon;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }
}
