package lucian.leagueassistant.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import cz.msebera.android.httpclient.Header;
import lucian.leagueassistant.R;
import lucian.leagueassistant.model.Icon;
import lucian.leagueassistant.model.Summoner;
import lucian.leagueassistant.service.DatabaseHandler;
import lucian.leagueassistant.service.LeagueAssistantAPI;

/**
 * Created by Lucian on 07-Mar-17.
 */

public class LoginActivity extends Activity {

    private Spinner spinner;
    private ArrayAdapter<CharSequence> adapter;

    private EditText summonerNameText;
    private String summonerName;
    private String region;
    private Map<String, String> regionMap = new HashMap<String, String>();
    private DatabaseHandler databaseHandler = new DatabaseHandler(this);


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        spinner = (Spinner) findViewById(R.id.regionSpinner);
        adapter = ArrayAdapter.createFromResource(this, R.array.server_names, R.layout.spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        regionMap = new HashMap<>();
        regionMap.put("EU Nordic & East", "EUNE");
        regionMap.put("EU West", "EUW");
        regionMap.put("Russia", "RU");
        regionMap.put("Korea", "KR");
        regionMap.put("Brazil", "BR");
        regionMap.put("Oceania", "OCE");
        regionMap.put("Japan", "JP");
        regionMap.put("North America", "NA");
        regionMap.put("Turkey", "TR");
        regionMap.put("Latin America North", "LAN");
        regionMap.put("Latin America South", "LAS");

    }

    public void verify(View view) {
        final TextView errorText = (TextView) findViewById(R.id.errorText);
        final ProgressBar progressBar = (ProgressBar) findViewById(R.id.progressBar);

        summonerNameText = (EditText) findViewById(R.id.summonerNameInput);
        summonerName = summonerNameText.getText().toString();
        region = regionMap.get(spinner.getSelectedItem());
        progressBar.setVisibility(View.VISIBLE);

        RequestParams requestParams = new RequestParams();
        requestParams.put("region", region);
        requestParams.put("name", summonerName);
        LeagueAssistantAPI.get("leagueAssistant/summoner", requestParams, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                if (statusCode == 200) {
                    try {
                        final Summoner summoner = new Summoner(response.getJSONObject("key").getInt("id"), response.getString("name"), region, response.getLong("accountId"), response.getInt("profileIconId"), 0);
                        databaseHandler.addSummoner(summoner);
                        Icon icon = databaseHandler.getSummonerIcon(summoner.getProfileIconId());
                        if (icon == null) {
                            final RequestParams requestParams = new RequestParams();
                            requestParams.put("region", region);
                            requestParams.put("id", summoner.getProfileIconId());
                            LeagueAssistantAPI.get("leagueAssistant/icons/summoner", requestParams, new AsyncHttpResponseHandler() {
                                @Override
                                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                                    Icon saveIcon = new Icon(summoner.getProfileIconId(), responseBody);
                                    databaseHandler.addSummonerIcon(saveIcon);
                                }

                                @Override
                                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                                }
                            });
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    progressBar.setVisibility(View.INVISIBLE);
                    finish();
                } else if (statusCode == 204) {
                    errorText.setText("Summoner Not Found");
                    progressBar.setVisibility(View.INVISIBLE);
                } else {
                    errorText.setText("Error. Try again");
                    progressBar.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String response, Throwable throwable) {
                if (statusCode == 0) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
                    builder.setMessage("Please check your internet connection and try again").setTitle("Error");
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                        }
                    });
                    AlertDialog dialog = builder.create();
                    dialog.show();
                }
                errorText.setText("Error. Try again");
                progressBar.setVisibility(View.INVISIBLE);
            }
        });


    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
        databaseHandler.close();
    }
}