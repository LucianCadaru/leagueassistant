package lucian.leagueassistant.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.gson.Gson;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONObject;

import java.util.List;

import cz.msebera.android.httpclient.Header;
import lucian.leagueassistant.R;
import lucian.leagueassistant.model.Champion;
import lucian.leagueassistant.model.ChampionBuild;
import lucian.leagueassistant.model.LiveGameParticipant;
import lucian.leagueassistant.model.Spell;
import lucian.leagueassistant.model.Summoner;
import lucian.leagueassistant.service.DatabaseHandler;
import lucian.leagueassistant.service.GameCache;
import lucian.leagueassistant.service.LeagueAssistantAPI;

/**
 * Created by Lucian on 15-Jun-17.
 */

public class LiveGameTab1Fragment extends Fragment {

    private GameCache gameCache = GameCache.getInstance();
    private DatabaseHandler databaseHandler;
    private long summonerId;
    private int summonerChampId;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View V = inflater.inflate(R.layout.live_game_tab1, container, false);
        return V;
    }

    @Override
    public void onResume() {
        super.onResume();
        final ImageView champImg = (ImageView) getActivity().findViewById(R.id.champBtn);
        databaseHandler = new DatabaseHandler(getContext());
        summonerId = getActivity().getIntent().getLongExtra("summonerId", 0L);
        final Summoner summoner = databaseHandler.getSummoner(summonerId);
        if (gameCache.getCache(summonerId) == null)
            getActivity().finish();
        else {
            for (LiveGameParticipant participant : gameCache.getCache(summonerId).getLiveGame().getParticipants()) {
                if (participant.getSummonerId() == summonerId)
                    summonerChampId = participant.getChampionId();
            }
            Champion champion;
            final View tab1View = getView();
            if (databaseHandler.getChampion(summonerChampId) == null) {
                RequestParams requestParams = new RequestParams();
                requestParams.put("region", summoner.getRegion());
                requestParams.put("id", summonerChampId);
                LeagueAssistantAPI.get("leagueAssistant/champion", requestParams, new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject responseBody) {
                        Gson gson = new Gson();
                        final Champion champion = gson.fromJson(responseBody.toString(), Champion.class);
                        TextView skillsTextView = (TextView) getActivity().findViewById(R.id.skillTxtView);
                        skillsTextView.setText("Best performing build with " + champion.getName());
                        getChampionIcon(champImg, champion.getKey());
                        Log.v("TAG", champion.getKey());
                        getChampionSpells(champion.getSpells());

                        LeagueAssistantAPI.get("leagueAssistant/builds/" + champion.getId(), null, new JsonHttpResponseHandler() {
                            @Override
                            public void onSuccess(int statusCode, Header[] headers, JSONObject responseBody) {
                                Gson gson = new Gson();
                                ChampionBuild championBuild = gson.fromJson(responseBody.toString(), ChampionBuild.class);
                                setSkills(championBuild, tab1View);
                                setItems(championBuild, tab1View);
                            }
                        });
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String response, Throwable throwable) {
                        if (statusCode == 0) {
                            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                            builder.setMessage("Please check your internet connection and try again").setTitle("Error");
                            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                }
                            });
                            AlertDialog dialog = builder.create();
                            dialog.show();
                        }
                    }
                });


            } else {
                champion = databaseHandler.getChampion(summonerChampId);
                TextView skillsTextView = (TextView) getActivity().findViewById(R.id.skillTxtView);
                skillsTextView.setText("Best performing build with " + champion.getName());
                getChampionIcon((ImageView) getActivity().findViewById(R.id.champBtn), champion.getKey());
                getChampionSpells(champion.getSpells());
                LeagueAssistantAPI.get("leagueAssistant/builds/" + champion.getId(), null, new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject responseBody) {
                        Gson gson = new Gson();
                        ChampionBuild championBuild = gson.fromJson(responseBody.toString(), ChampionBuild.class);
                        setSkills(championBuild, tab1View);
                        setItems(championBuild, tab1View);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String response, Throwable throwable) {
                        if (statusCode == 0) {
                            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                            builder.setMessage("Please check your internet connection and try again").setTitle("Error");
                            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                }
                            });
                            AlertDialog dialog = builder.create();
                            dialog.show();
                        }
                    }
                });
            }

        }
    }

    private void setSkills(ChampionBuild championBuild, View view) {
        String skillHash = championBuild.getSkillsHash();
        String[] skillsArray = skillHash.split("-");
        ImageView imageView;
        for (int i = 1; i < skillsArray.length; i++) {
            if (skillsArray[i].contentEquals("Q")) {
                imageView = (ImageView) view.findViewWithTag("q_" + i);
                imageView.setVisibility(View.VISIBLE);
            }
            if (skillsArray[i].contentEquals("W")) {
                imageView = (ImageView) view.findViewWithTag("w_" + i);
                imageView.setVisibility(View.VISIBLE);
            }
            if (skillsArray[i].contentEquals("E")) {
                imageView = (ImageView) view.findViewWithTag("e_" + i);
                imageView.setVisibility(View.VISIBLE);
            }
            if (skillsArray[i].contentEquals("R")) {
                imageView = (ImageView) view.findViewWithTag("r_" + i);
                imageView.setVisibility(View.VISIBLE);
            }

        }
    }

    private void setItems(ChampionBuild championBuild, View view) {
        String itemsHash = championBuild.getItemsHash();
        String[] itemsArray = itemsHash.split("-");
        ImageView imageView = null;
        for (int i = 1; i < itemsArray.length; i++) {
            if (i == 1)
                imageView = (ImageView) view.findViewById(R.id.item1);
            if (i == 2)
                imageView = (ImageView) view.findViewById(R.id.item2);
            if (i == 3)
                imageView = (ImageView) view.findViewById(R.id.item3);
            if (i == 4)
                imageView = (ImageView) view.findViewById(R.id.item4);
            if (i == 5)
                imageView = (ImageView) view.findViewById(R.id.item5);
            if (i == 6)
                imageView = (ImageView) view.findViewById(R.id.item6);

            getItemsIcon(Integer.valueOf(itemsArray[i]), imageView);
        }
        ImageView arrow1 = (ImageView) view.findViewById(R.id.arrow1);
        ImageView arrow2 = (ImageView) view.findViewById(R.id.arrow2);
        ImageView arrow3 = (ImageView) view.findViewById(R.id.arrow3);
        ImageView arrow4 = (ImageView) view.findViewById(R.id.arrow4);
        ImageView arrow5 = (ImageView) view.findViewById(R.id.arrow5);
        arrow1.setVisibility(View.VISIBLE);
        arrow2.setVisibility(View.VISIBLE);
        arrow3.setVisibility(View.VISIBLE);
        arrow4.setVisibility(View.VISIBLE);
        arrow5.setVisibility(View.VISIBLE);


    }

    public void getChampionIcon(ImageView imageView, String key) {
        //TODO: GET LATEST VERSION
        Glide.with(getActivity())
                .load("http://ddragon.leagueoflegends.com/cdn/7.12.1/img/champion/" + key + ".png")
                .transform(new CircularTransformation(getContext()))
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(imageView);
    }

    public void getChampionSpells(List<Spell> spells) {
        //TODO: GET LATEST VERSION
        ImageView imageView = null;
        for (int i = 0; i < spells.size(); i++) {
            if (i == 0)
                imageView = (ImageView) getActivity().findViewById(R.id.q_spell);
            if (i == 1)
                imageView = (ImageView) getActivity().findViewById(R.id.w_spell);
            if (i == 2)
                imageView = (ImageView) getActivity().findViewById(R.id.e_spell);
            if (i == 3)
                imageView = (ImageView) getActivity().findViewById(R.id.r_spell);

            Glide.with(getActivity())
                    .load("http://ddragon.leagueoflegends.com/cdn/7.12.1/img/spell/" + spells.get(i).getKey() + ".png")
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .bitmapTransform(new RoundedCornersTransformation(getActivity(), 10, 2))
                    .into(imageView);
        }
    }

    public void getItemsIcon(int itemId, ImageView imageView) {
        //TODO: GET LATEST VERSION
        Glide.with(getActivity())
                .load("http://ddragon.leagueoflegends.com/cdn/7.12.1/img/item/" + itemId + ".png")
                .bitmapTransform(new RoundedCornersTransformation(getActivity(), 10, 2))
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(imageView);

    }

}
