package lucian.leagueassistant.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;
import lucian.leagueassistant.R;
import lucian.leagueassistant.model.ChampionBuild;
import lucian.leagueassistant.model.ItemDto;
import lucian.leagueassistant.model.LiveGameParticipant;
import lucian.leagueassistant.service.DatabaseHandler;
import lucian.leagueassistant.service.GameCache;
import lucian.leagueassistant.service.LeagueAssistantAPI;

public class LiveGameStatsActivity extends FragmentActivity {

    private GameCache gameCache = GameCache.getInstance();
    private ChampionBuild championBuild;
    private DatabaseHandler databaseHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_live_game_stats);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);


        ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);
        TabAdapter adapter = new TabAdapter(this, getSupportFragmentManager());
        viewPager.setAdapter(adapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.sliding_tabs);
        tabLayout.setupWithViewPager(viewPager);

        databaseHandler = new DatabaseHandler(getBaseContext());
        long summonerId = getIntent().getLongExtra("summonerId", 0L);
        int summonerChampId = 0;
        if (gameCache.getCache(summonerId) != null) {
            for (LiveGameParticipant participant : gameCache.getCache(summonerId).getLiveGame().getParticipants()) {
                if (participant.getSummonerId() == summonerId)
                    summonerChampId = participant.getChampionId();
            }

            LeagueAssistantAPI.get("leagueAssistant/builds/" + summonerChampId, null, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject responseBody) {
                    Gson gson = new Gson();
                    championBuild = gson.fromJson(responseBody.toString(), ChampionBuild.class);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String response, Throwable throwable) {
                    if (statusCode == 0) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(LiveGameStatsActivity.this);
                        builder.setMessage("Please check your internet connection and try again").setTitle("Error");
                        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                            }
                        });
                        AlertDialog dialog = builder.create();
                        dialog.show();
                    }
                }
            });
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        databaseHandler.close();
    }

    public void details(View view) {
        Intent details = new Intent(getBaseContext(), Details.class);
        long summonerId = getIntent().getLongExtra("summonerId", 0L);
        View row = (View) view.getParent();
        TextView summonerView = (TextView) row.findViewById(R.id.summonerListName);
        details.putExtra("summonerId", summonerId);
        details.putExtra("summonerName", summonerView.getText());
        startActivity(details);
    }


    public void getItemDetails(View view) {
        int itemNumber = Integer.valueOf(getResources().getResourceEntryName(view.getId()).substring(4));
        String itemsHash = championBuild.getItemsHash();
        String[] itemsArray = itemsHash.split("-");
        ItemDto itemDto = databaseHandler.getItem(Integer.valueOf(itemsArray[itemNumber]));
        String message = String.valueOf(itemDto.getName() + "\n\n" + itemDto.getGold() + " gold" + "\n\n" + Html.fromHtml(itemDto.getDescription()));
        Toast toast = Toast.makeText(view.getContext(), message, Toast.LENGTH_LONG);
        toast.show();


    }

}
