package lucian.leagueassistant.activity;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import lucian.leagueassistant.R;

/**
 * Created by Lucian on 18-Jun-17.
 */
public class TabAdapter extends FragmentPagerAdapter {

    private Context mContext;
    private LiveGameTab1Fragment tab1Fragment;
    private LiveGameTab2Fragment tab2Fragment;
    private LiveGameTab3Fragment tab3Fragment;

    public TabAdapter(Context context, FragmentManager fm) {
        super(fm);
        mContext = context;
    }

    @Override
    public Fragment getItem(int position) {
        if (position == 0) {
            if (tab1Fragment == null) {
                tab1Fragment = new LiveGameTab1Fragment();
                return tab1Fragment;
            } else return tab1Fragment;
        } else if (position == 1) {
            if (tab2Fragment == null) {
                tab2Fragment = new LiveGameTab2Fragment();
                return tab2Fragment;
            } else return tab2Fragment;
        } else {
            if (tab3Fragment == null) {
                tab3Fragment = new LiveGameTab3Fragment();
                return tab3Fragment;
            } else return tab3Fragment;
        }
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "You";
            case 1:
                return "Your Team";
            case 2:
                return "Your Enemies";
            default:
                return null;
        }
    }

}