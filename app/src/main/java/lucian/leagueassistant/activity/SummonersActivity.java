package lucian.leagueassistant.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;

import lucian.leagueassistant.R;
import lucian.leagueassistant.model.Summoner;
import lucian.leagueassistant.service.DatabaseHandler;

/**
 * Created by Lucian on 13-Jun-17.
 */

public class SummonersActivity extends Activity {

    private ListView summonersList;
    private SummonerListAdapter adapter;
    private DatabaseHandler databaseHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_summoners);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        databaseHandler = new DatabaseHandler(getBaseContext());
        summonersList = (ListView) findViewById(R.id.summonersList);
        adapter = new SummonerListAdapter(this, databaseHandler);
        summonersList.setAdapter(adapter);
    }

    public void addSummoner(View view) {
        Intent myIntent = new Intent(this, LoginActivity.class);
        startActivity(myIntent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        adapter.update();
    }

    protected void removeSummoner(View view) {
        View view1 = (View) view.getParent();
        String name = ((TextView) view1.findViewById(R.id.summonerName)).getText().toString();
        String region = ((TextView) view1.findViewById(R.id.region)).getText().toString();
        final Summoner summoner = databaseHandler.getSummoner(name, region);
        if (summoner.isSelected()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(SummonersActivity.this);
            builder.setMessage("You can't delete a selected summoner").setTitle("Error");
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                }
            });
            AlertDialog dialog = builder.create();
            dialog.show();
        } else {

            AlertDialog.Builder builder = new AlertDialog.Builder(SummonersActivity.this);
            builder.setMessage("Are you sure you want to delete summoner " + name).setTitle("Delete");
            builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    databaseHandler.removeSummoner(summoner);
                    adapter.update();
                }
            });
            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                }
            });
            AlertDialog dialog = builder.create();
            dialog.show();
        }
    }

    protected void newSelected(View view) {
        View view1 = (View) view.getParent();
        List<Summoner> summoners = databaseHandler.getAllSummoners();

        if (summoners.size() == 1) {
            AlertDialog.Builder builder = new AlertDialog.Builder(SummonersActivity.this);
            builder.setMessage("Summoner is already selected").setTitle("Error");
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                }
            });
            AlertDialog dialog = builder.create();
            dialog.show();
        }
        if (summoners.size() > 1) {
            String name = ((TextView) view1.findViewById(R.id.summonerName)).getText().toString();
            String region = ((TextView) view1.findViewById(R.id.region)).getText().toString();
            Summoner summoner = databaseHandler.getSummoner(name, region);
            if (summoner.isSelected()) {
                AlertDialog.Builder builder = new AlertDialog.Builder(SummonersActivity.this);
                builder.setMessage("Summoner is already selected").setTitle("Error");
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });
                AlertDialog dialog = builder.create();
                dialog.show();
            } else {
                for (Summoner summoner1 : summoners) {
                    if (summoner1.isSelected()) {
                        summoner1.setSelected(false);
                        databaseHandler.updateSummoner(summoner1);
                    }
                }
                summoner.setSelected(true);
                databaseHandler.updateSummoner(summoner);

            }
        }
        adapter.update();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        databaseHandler.close();
    }

}
