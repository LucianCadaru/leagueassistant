package lucian.leagueassistant.activity;

import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.HorizontalScrollView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.ArrayList;
import java.util.List;

import lucian.leagueassistant.R;
import lucian.leagueassistant.model.Champion;
import lucian.leagueassistant.model.LiveGame;
import lucian.leagueassistant.model.LiveGameParticipant;
import lucian.leagueassistant.model.Mastery;
import lucian.leagueassistant.model.MasteryDto;
import lucian.leagueassistant.model.Rune;
import lucian.leagueassistant.model.RuneDto;
import lucian.leagueassistant.service.DatabaseHandler;
import lucian.leagueassistant.service.GameCache;

public class Details extends Activity {

    private GameCache gameCache = GameCache.getInstance();
    private DatabaseHandler databaseHandler;
    private LiveGameParticipant participant;
    private List<Rune> runes;
    private List<Mastery> masteries;

    private List<Rune> marks = new ArrayList<>();
    private List<Rune> seals = new ArrayList<>();
    private List<Rune> glyphs = new ArrayList<>();
    private List<Rune> quints = new ArrayList<>();

    @Override
    protected void onDestroy() {
        super.onDestroy();
        databaseHandler.close();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_summoner_details);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);


        databaseHandler = new DatabaseHandler(getBaseContext());
        long summonerId = getIntent().getLongExtra("summonerId", 0L);
        String summonerName = getIntent().getStringExtra("summonerName");
        if (gameCache.getCache(summonerId) == null) {
            finish();
        } else {
            LiveGame liveGame = gameCache.getCache(summonerId).getLiveGame();
            for (LiveGameParticipant participant : liveGame.getParticipants()) {
                if (participant.getSummonerName().contentEquals(summonerName)) {
                    this.participant = participant;
                    break;
                }
            }
            TextView currentSummoner = (TextView) findViewById(R.id.currentSummoner);
            currentSummoner.setText(summonerName);

            ImageView championIcon = (ImageView) findViewById(R.id.champImgDetail);
            Champion champion = databaseHandler.getChampion(participant.getChampionId());
            getChampionIcon(championIcon, champion.getKey());

            runes = participant.getRunes();
            masteries = participant.getMasteries();

            LinearLayout tables = (LinearLayout) findViewById(R.id.tables);
            setSaturated(tables);
            setMasteries(tables, masteries);
            setRunes(runes);
        }
    }

    public void setRunes(List<Rune> runes) {
        RuneDto runeDto;
        TextView nullRunes = (TextView) findViewById(R.id.nullRunes);
        HorizontalScrollView runesView = (HorizontalScrollView) findViewById(R.id.runeScrollView);
        if (runes.isEmpty()) {
            nullRunes.setVisibility(View.VISIBLE);
            nullRunes.setText(this.participant.getSummonerName() + " has no runes");
            runesView.setVisibility(View.GONE);
        } else {
            for (Rune rune : runes) {
                runeDto = databaseHandler.getRune(rune.getRuneId());
                if (runeDto.getType().contentEquals("red")) {
                    marks.add(rune);
                }
                if (runeDto.getType().contentEquals("yellow")) {
                    seals.add(rune);
                }
                if (runeDto.getType().contentEquals("blue")) {
                    glyphs.add(rune);
                }
                if (runeDto.getType().contentEquals("black")) {
                    quints.add(rune);
                }
            }

            ImageButton mark = null;
            TextView count = null;
            for (int i = 0; i < marks.size(); i++) {
                switch (i) {
                    case 0: {
                        mark = (ImageButton) runesView.findViewById(R.id.mark1);
                        count = (TextView) findViewById(R.id.countM1);
                        break;
                    }

                    case 1: {
                        mark = (ImageButton) runesView.findViewById(R.id.mark2);
                        count = (TextView) findViewById(R.id.countM2);
                        break;
                    }

                    case 2: {
                        mark = (ImageButton) runesView.findViewById(R.id.mark3);
                        count = (TextView) findViewById(R.id.countM3);
                        break;
                    }

                    case 3: {
                        mark = (ImageButton) runesView.findViewById(R.id.mark4);
                        count = (TextView) findViewById(R.id.countM4);
                        break;
                    }

                    case 4: {
                        mark = (ImageButton) runesView.findViewById(R.id.mark5);
                        count = (TextView) findViewById(R.id.countM5);
                        break;
                    }
                    case 5: {
                        mark = (ImageButton) runesView.findViewById(R.id.mark6);
                        count = (TextView) findViewById(R.id.countM7);
                        break;
                    }
                    case 6: {
                        mark = (ImageButton) runesView.findViewById(R.id.mark7);
                        count = (TextView) findViewById(R.id.countM7);
                        break;
                    }
                    case 7: {
                        mark = (ImageButton) runesView.findViewById(R.id.mark8);
                        count = (TextView) findViewById(R.id.countM8);
                        break;
                    }
                    case 8: {
                        mark = (ImageButton) runesView.findViewById(R.id.mark9);
                        count = (TextView) findViewById(R.id.countM9);
                        break;
                    }
                }
                mark.setVisibility(View.VISIBLE);
                count.setVisibility(View.VISIBLE);
                count.setText(String.valueOf(marks.get(i).getCount()));
                getRune(mark, databaseHandler.getRune(marks.get(i).getRuneId()).getKey());
            }


            ImageButton seal = null;
            for (int i = 0; i < seals.size(); i++) {
                switch (i) {
                    case 0: {
                        seal = (ImageButton) runesView.findViewById(R.id.seal1);
                        count = (TextView) findViewById(R.id.countS1);
                        break;
                    }

                    case 1: {
                        seal = (ImageButton) runesView.findViewById(R.id.seal2);
                        count = (TextView) findViewById(R.id.countS2);
                        break;
                    }

                    case 2: {
                        seal = (ImageButton) runesView.findViewById(R.id.seal3);
                        count = (TextView) findViewById(R.id.countS3);
                        break;
                    }

                    case 3: {
                        seal = (ImageButton) runesView.findViewById(R.id.seal4);
                        count = (TextView) findViewById(R.id.countS4);
                        break;
                    }

                    case 4: {
                        seal = (ImageButton) runesView.findViewById(R.id.seal5);
                        count = (TextView) findViewById(R.id.countS5);
                        break;
                    }
                    case 5: {
                        seal = (ImageButton) runesView.findViewById(R.id.seal6);
                        count = (TextView) findViewById(R.id.countS6);
                        break;
                    }
                    case 6: {
                        seal = (ImageButton) runesView.findViewById(R.id.seal7);
                        count = (TextView) findViewById(R.id.countS7);
                        break;
                    }
                    case 7: {
                        seal = (ImageButton) runesView.findViewById(R.id.seal8);
                        count = (TextView) findViewById(R.id.countS8);
                        break;
                    }
                    case 8: {
                        seal = (ImageButton) runesView.findViewById(R.id.seal9);
                        count = (TextView) findViewById(R.id.countS9);
                        break;
                    }
                }
                seal.setVisibility(View.VISIBLE);
                count.setVisibility(View.VISIBLE);
                count.setText(String.valueOf(seals.get(i).getCount()));
                getRune(seal, databaseHandler.getRune(seals.get(i).getRuneId()).getKey());
            }

            ImageButton glyph = null;
            for (int i = 0; i < glyphs.size(); i++) {
                switch (i) {
                    case 0: {
                        glyph = (ImageButton) runesView.findViewById(R.id.glyph1);
                        count = (TextView) findViewById(R.id.countG1);
                        break;
                    }

                    case 1: {
                        glyph = (ImageButton) runesView.findViewById(R.id.glyph2);
                        count = (TextView) findViewById(R.id.countG2);
                        break;
                    }

                    case 2: {
                        glyph = (ImageButton) runesView.findViewById(R.id.glyph3);
                        count = (TextView) findViewById(R.id.countG3);
                        break;
                    }

                    case 3: {
                        glyph = (ImageButton) runesView.findViewById(R.id.glyph4);
                        count = (TextView) findViewById(R.id.countG4);
                        break;
                    }

                    case 4: {
                        glyph = (ImageButton) runesView.findViewById(R.id.glyph5);
                        count = (TextView) findViewById(R.id.countG5);
                        break;
                    }
                    case 5: {
                        glyph = (ImageButton) runesView.findViewById(R.id.glyph6);
                        count = (TextView) findViewById(R.id.countG6);
                        break;
                    }
                    case 6: {
                        glyph = (ImageButton) runesView.findViewById(R.id.glyph7);
                        count = (TextView) findViewById(R.id.countG7);
                        break;
                    }
                    case 7: {
                        glyph = (ImageButton) runesView.findViewById(R.id.glyph8);
                        count = (TextView) findViewById(R.id.countG8);
                        break;
                    }
                    case 8: {
                        glyph = (ImageButton) runesView.findViewById(R.id.glyph9);
                        count = (TextView) findViewById(R.id.countG9);
                        break;
                    }
                }
                count.setVisibility(View.VISIBLE);
                count.setText(String.valueOf(glyphs.get(i).getCount()));
                glyph.setVisibility(View.VISIBLE);
                getRune(glyph, databaseHandler.getRune(glyphs.get(i).getRuneId()).getKey());
            }

            ImageButton quint = null;
            for (int i = 0; i < quints.size(); i++) {
                switch (i) {
                    case 0: {
                        quint = (ImageButton) runesView.findViewById(R.id.quintessence1);
                        count = (TextView) findViewById(R.id.countQ1);
                        break;
                    }

                    case 1: {
                        quint = (ImageButton) runesView.findViewById(R.id.quintessence2);
                        count = (TextView) findViewById(R.id.countQ2);
                        break;
                    }

                    case 2: {
                        quint = (ImageButton) runesView.findViewById(R.id.quintessence3);
                        count = (TextView) findViewById(R.id.countQ3);
                        break;
                    }
                }
                count.setVisibility(View.VISIBLE);
                count.setText(String.valueOf(quints.get(i).getCount()));
                quint.setVisibility(View.VISIBLE);
                getRune(quint, databaseHandler.getRune(quints.get(i).getRuneId()).getKey());
            }
        }

    }

    public void setMasteries(LinearLayout tables, List<Mastery> masteries) {

        ColorMatrix matrix2 = new ColorMatrix();
        matrix2.setSaturation(1);
        ColorMatrixColorFilter saturated = new ColorMatrixColorFilter(matrix2);
        TextView txtMastery;
        ImageView imageMastery;

        for (Mastery mastery : masteries) {
            txtMastery = (TextView) tables.findViewWithTag("txt" + String.valueOf(mastery.getMasteryId()));
            if (txtMastery != null) {
                txtMastery.setText(String.valueOf(mastery.getRank()) + "/5");
                txtMastery.setVisibility(View.VISIBLE);
            }
            imageMastery = (ImageView) tables.findViewWithTag("mastery" + String.valueOf(mastery.getMasteryId()));
            if (imageMastery != null) {
                imageMastery.setColorFilter(saturated);
            }
        }
    }


    private void setSaturated(LinearLayout tables) {
        List<Integer> allMasteriesId = new ArrayList<>();
        allMasteriesId.add(6211);
        allMasteriesId.add(6212);
        allMasteriesId.add(6221);
        allMasteriesId.add(6222);
        allMasteriesId.add(6223);
        allMasteriesId.add(6231);
        allMasteriesId.add(6232);
        allMasteriesId.add(6241);
        allMasteriesId.add(6242);
        allMasteriesId.add(6243);
        allMasteriesId.add(6251);
        allMasteriesId.add(6252);
        allMasteriesId.add(6261);
        allMasteriesId.add(6262);
        allMasteriesId.add(6263);

        allMasteriesId.add(6311);
        allMasteriesId.add(6312);
        allMasteriesId.add(6321);
        allMasteriesId.add(6322);
        allMasteriesId.add(6323);
        allMasteriesId.add(6331);
        allMasteriesId.add(6332);
        allMasteriesId.add(6341);
        allMasteriesId.add(6342);
        allMasteriesId.add(6343);
        allMasteriesId.add(6351);
        allMasteriesId.add(6352);
        allMasteriesId.add(6361);
        allMasteriesId.add(6362);
        allMasteriesId.add(6363);

        allMasteriesId.add(6111);
        allMasteriesId.add(6114);
        allMasteriesId.add(6121);
        allMasteriesId.add(6122);
        allMasteriesId.add(6123);
        allMasteriesId.add(6134);
        allMasteriesId.add(6131);
        allMasteriesId.add(6141);
        allMasteriesId.add(6142);
        allMasteriesId.add(6143);
        allMasteriesId.add(6151);
        allMasteriesId.add(6154);
        allMasteriesId.add(6161);
        allMasteriesId.add(6162);
        allMasteriesId.add(6164);

        ColorMatrix matrix = new ColorMatrix();
        matrix.setSaturation(0);
        ColorMatrixColorFilter desaturated = new ColorMatrixColorFilter(matrix);

        ImageView masteryImage;
        for (Integer integer : allMasteriesId) {
            masteryImage = (ImageView) tables.findViewWithTag("mastery" + String.valueOf(integer));
            masteryImage.setColorFilter(desaturated);
        }
    }

    public void getRuneInfo(View view) {
        RuneDto runeDto = null;
        String id = getResources().getResourceEntryName(view.getId());
        String category = id.substring(0, id.length() - 1);
        int iId = Integer.valueOf(id.substring(id.length() - 1)) - 1;
        if (category.contentEquals("mark"))
            runeDto = databaseHandler.getRune(marks.get(iId).getRuneId());
        if (category.contentEquals("seal"))
            runeDto = databaseHandler.getRune(seals.get(iId).getRuneId());
        if (category.contentEquals("glyph"))
            runeDto = databaseHandler.getRune(glyphs.get(iId).getRuneId());
        if (category.contentEquals("quintessence"))
            runeDto = databaseHandler.getRune(quints.get(iId).getRuneId());
        String message = String.valueOf(runeDto.getName() + "\n\nEach one gives " + Html.fromHtml(runeDto.getDescription()));
        Toast toast = Toast.makeText(view.getContext(), message, Toast.LENGTH_SHORT);
        toast.show();
    }

    public void getMasteryInfo(View view) {
        MasteryDto masteryDto = null;
        String id = getResources().getResourceEntryName(view.getId());
        int iId = Integer.valueOf(id.substring(id.length() - 4));
        for (int i = 0; i < masteries.size(); i++) {
            if (masteries.get(i).getMasteryId() == iId) {
                masteryDto = databaseHandler.getMastery(iId);
                String message = String.valueOf(masteryDto.getName() + "\n\n" + Html.fromHtml(masteryDto.getDescription()[masteries.get(i).getRank() - 1]));
                Toast toast = Toast.makeText(view.getContext(), message, Toast.LENGTH_SHORT);
                toast.show();
                return;
            }
        }

        masteryDto = databaseHandler.getMastery(iId);
        String message = String.valueOf(masteryDto.getName() + "\n\n" + Html.fromHtml(masteryDto.getDescription()[0]));
        Toast toast = Toast.makeText(view.getContext(), message, Toast.LENGTH_SHORT);
        toast.show();


    }

    private void getChampionIcon(ImageView imageView, String key) {
        //TODO: GET LATEST VERSION
        Glide.with(getBaseContext())
                .load("http://ddragon.leagueoflegends.com/cdn/7.12.1/img/champion/" + key + ".png")
                .transform(new CircularTransformation(getBaseContext()))
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(imageView);
    }

    private void getRune(ImageButton imageButton, String key) {
        //TODO: GET LATEST VERSION
        Glide.with(getBaseContext())
                .load("http://ddragon.leagueoflegends.com/cdn/7.12.1/img/rune/" + key)
                .transform(new CircularTransformation(getBaseContext()))
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(imageButton);
    }
}
