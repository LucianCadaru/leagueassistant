package lucian.leagueassistant.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONObject;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import cz.msebera.android.httpclient.Header;
import lucian.leagueassistant.R;
import lucian.leagueassistant.model.Icon;
import lucian.leagueassistant.model.ItemDto;
import lucian.leagueassistant.model.LiveGame;
import lucian.leagueassistant.model.LiveGameParticipant;
import lucian.leagueassistant.model.MasteryDto;
import lucian.leagueassistant.model.RuneDto;
import lucian.leagueassistant.model.Summoner;
import lucian.leagueassistant.service.DatabaseHandler;
import lucian.leagueassistant.service.GameCache;
import lucian.leagueassistant.service.GameCacheable;
import lucian.leagueassistant.service.GameCacheableImpl;
import lucian.leagueassistant.service.LeagueAssistantAPI;


public class MainActivity extends Activity {

    private String summonerName;
    private String region;
    private int iconId;
    private long selected = 0;
    private DatabaseHandler databaseHandler;
    private GameCache gameCache = GameCache.getInstance();

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        databaseHandler = new DatabaseHandler(getBaseContext());
        List<Summoner> summoners = databaseHandler.getAllSummoners();
        for (Summoner summoner : summoners) {
            if (summoner.isSelected()) {
                selected = summoners.get(0).getId();
                summonerName = summoners.get(0).getName();
                region = summoners.get(0).getRegion();
            }
        }
        if (summoners.size() > 0 && selected == 0) {
            selected = summoners.get(0).getId();
            summoners.get(0).setSelected(true);
            databaseHandler.updateSummoner(summoners.get(0));
            summonerName = summoners.get(0).getName();
            region = summoners.get(0).getRegion();
        }


        if (databaseHandler.isRunesEmpty()) {
            if (!isNetworkAvailable()) {
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setMessage("Please check your internet connection and try again").setTitle("Error");
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });
                AlertDialog dialog = builder.create();
                dialog.show();
            } else {
                LeagueAssistantAPI.get("leagueAssistant/data/runes/NA", null, new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject responseBody) {
                        if (statusCode == 200) {
                            JsonParser jsonParser = new JsonParser();
                            JsonElement jsonElement = jsonParser.parse(responseBody.toString());
                            JsonObject jsonData = jsonElement.getAsJsonObject().get("data").getAsJsonObject();
                            Set<Map.Entry<String, JsonElement>> entries = jsonData.entrySet();
                            Map.Entry<String, JsonElement> runeEntry;
                            Iterator iter = entries.iterator();
                            while (iter.hasNext()) {
                                runeEntry = (Map.Entry<String, JsonElement>) iter.next();
                                JsonObject rune = runeEntry.getValue().getAsJsonObject();
                                RuneDto runeDto = new RuneDto();
                                runeDto.setId(rune.get("id").getAsInt());
                                runeDto.setName(rune.get("name").getAsString());
                                runeDto.setDescription(rune.get("description").getAsString());
                                runeDto.setType(rune.get("rune").getAsJsonObject().get("type").getAsString());
                                runeDto.setKey(rune.get("image").getAsJsonObject().get("full").getAsString());
                                databaseHandler.addRune(runeDto);

                            }
                        } else {
                            Log.v("response", String.valueOf(responseBody.toString()) + " " + String.valueOf(statusCode));
                        }
                    }
                });
            }
        }

        if (databaseHandler.isItemsEmpty()) {
            if (!isNetworkAvailable()) {
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setMessage("Please check your internet connection and try again").setTitle("Error");
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });
                AlertDialog dialog = builder.create();
                dialog.show();
            } else {
                LeagueAssistantAPI.get("leagueAssistant/data/items/NA", null, new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject responseBody) {
                        if (statusCode == 200) {
                            JsonParser jsonParser = new JsonParser();
                            JsonElement jsonElement = jsonParser.parse(responseBody.toString());
                            JsonObject jsonData = jsonElement.getAsJsonObject().get("data").getAsJsonObject();
                            Set<Map.Entry<String, JsonElement>> entries = jsonData.entrySet();
                            Map.Entry<String, JsonElement> itemEntry;
                            Iterator iter = entries.iterator();
                            while (iter.hasNext()) {
                                itemEntry = (Map.Entry<String, JsonElement>) iter.next();
                                JsonObject item = itemEntry.getValue().getAsJsonObject();

                                if (item.has("name") && item.has("description")) {
                                    ItemDto itemDto = new ItemDto();
                                    itemDto.setGold(item.get("gold").getAsJsonObject().get("total").getAsInt());
                                    itemDto.setId(item.get("id").getAsInt());
                                    itemDto.setName(item.get("name").getAsString());
                                    itemDto.setDescription(item.get("description").getAsString());
                                    databaseHandler.addItem(itemDto);
                                }

                            }
                        } else {
                            Log.v("response", String.valueOf(responseBody.toString()) + " " + String.valueOf(statusCode));
                        }
                    }

                });
            }
        }

        if (databaseHandler.isMasteriesEmpty()) {
            if (!isNetworkAvailable()) {
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setMessage("Please check your internet connection and try again").setTitle("Error");
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });
                AlertDialog dialog = builder.create();
                dialog.show();
            } else {
                LeagueAssistantAPI.get("leagueAssistant/data/masteries/NA", null, new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject responseBody) {
                        if (statusCode == 200) {
                            JsonParser jsonParser = new JsonParser();
                            JsonElement jsonElement = jsonParser.parse(responseBody.toString());
                            JsonObject jsonData = jsonElement.getAsJsonObject().get("data").getAsJsonObject();
                            Set<Map.Entry<String, JsonElement>> entries = jsonData.entrySet();
                            Map.Entry<String, JsonElement> itemEntry;
                            Iterator iter = entries.iterator();
                            while (iter.hasNext()) {
                                itemEntry = (Map.Entry<String, JsonElement>) iter.next();
                                JsonObject item = itemEntry.getValue().getAsJsonObject();
                                MasteryDto masteryDto = new MasteryDto();
                                masteryDto.setId(item.get("id").getAsInt());
                                masteryDto.setName(item.get("name").getAsString());
                                JsonArray jsonArray = item.get("description").getAsJsonArray();
                                String[] descriptions;
                                descriptions = new String[jsonArray.size()];
                                for (int i = 0; i < jsonArray.size(); i++) {
                                    descriptions[i] = jsonArray.get(i).getAsString();
                                }
                                masteryDto.setDescription(descriptions);
                                databaseHandler.addMastery(masteryDto);

                            }
                        } else {
                            Log.v("response", String.valueOf(responseBody.toString()) + " " + String.valueOf(statusCode));
                        }
                    }

                });
            }
        }


        Button summonersBtn = (Button) findViewById(R.id.summonersBtn);
        Bitmap bitmap1 = BitmapFactory.decodeResource(getResources(), R.drawable.default_summoner_icon);
        Drawable icon = new BitmapDrawable(getResources(), getCroppedBitmap(bitmap1));
        summonersBtn.setCompoundDrawablesWithIntrinsicBounds(icon, null, null, null);
        String btnText = "<font color='white'>" + summonerName + "</font> <font color='#153d3d'>NA" + "</font>";
        summonersBtn.setText(btnText);
        ProgressBar progressBar = (ProgressBar) findViewById(R.id.liveProgressBar);
        progressBar.setVisibility(View.INVISIBLE);
    }

    public void startLogin() {
        Intent myIntent = new Intent(this, LoginActivity.class);
        startActivity(myIntent);
    }

    public void summonersList(View view) {
        Intent myIntent = new Intent(this, SummonersActivity.class);
        startActivity(myIntent);
    }

    public void liveMatch(View view) {
        final Summoner summoner = databaseHandler.getSummoner(selected);
        final ProgressBar progressBar = (ProgressBar) findViewById(R.id.liveProgressBar);
        progressBar.setVisibility(View.VISIBLE);
        GameCacheable gameCacheable = gameCache.getCache(summoner.getId());
        if (gameCacheable == null) {
            if (!isNetworkAvailable()) {
                progressBar.setVisibility(View.GONE);
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setMessage("Please check your internet connection and try again").setTitle("Error");
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });
                AlertDialog dialog = builder.create();
                dialog.show();
                progressBar.setVisibility(View.GONE);
            } else {
                RequestParams requestParams = new RequestParams();
                requestParams.put("region", summoner.getRegion());
                requestParams.put("summonerId", summoner.getId());
                LeagueAssistantAPI.get("leagueAssistant/live", requestParams, new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject responseBody) {
                        if (statusCode == 204) {
                            AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                            builder.setMessage("You must be in a game.\nRemember that the service only works for matchmade games").setTitle("Error");
                            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                }
                            });
                            AlertDialog dialog = builder.create();
                            dialog.show();
                            progressBar.setVisibility(View.GONE);
                        } else if (statusCode == 200) {
                            Gson gson = new Gson();
                            LiveGame liveGame = gson.fromJson(responseBody.toString(), LiveGame.class);
                            GameCacheable gameCacheable = new GameCacheableImpl(liveGame, summoner.getId(), 10);
                            for (LiveGameParticipant participant: liveGame.getParticipants()){
                                if (participant.getSummonerId()==summoner.getId())
                                    if (participant.getProfileIconId()!=summoner.getProfileIconId()){
                                        setSummoner(participant.getSummonerName(),summoner.getRegion(),participant.getProfileIconId());
                                    }
                            }
                            gameCache.putCache(gameCacheable);
                            Intent myIntent = new Intent(getBaseContext(), LiveGameStatsActivity.class);
                            myIntent.putExtra("summonerId", summoner.getId());
                            startActivity(myIntent);
                            progressBar.setVisibility(View.GONE);
                        }
                    }


                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject jsonObject) {

                        progressBar.setVisibility(View.GONE);
                        if (statusCode == 500) {
                            AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                            builder.setMessage("We are some difficulties with our server. Try again later.").setTitle("Error");
                            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                }
                            });
                            AlertDialog dialog = builder.create();
                            dialog.show();
                            progressBar.setVisibility(View.GONE);
                        }
                    }
                });
            }

        } else {
            progressBar.setVisibility(View.GONE);
            Intent myIntent = new Intent(getBaseContext(), LiveGameStatsActivity.class);
            myIntent.putExtra("summonerId", summoner.getId());
            startActivity(myIntent);
        }

    }

    @Override
    protected void onResume() {
        super.onResume();

        List<Summoner> summoners = databaseHandler.getAllSummoners();
        if (summoners.size() > 0)
            for (Summoner summoner : summoners)
                if (summoner.isSelected())
                    selected = summoner.getId();


        if (selected != 0) {
            Summoner summoner = databaseHandler.getSummoner(selected);
            setSummoner(summoner.getName(),summoner.getRegion(),summoner.getProfileIconId());

        } else {
            if (summoners.size() > 0) {
                selected = summoners.get(0).getId();
                summoners.get(0).setSelected(true);
                databaseHandler.updateSummoner(summoners.get(0));
                setSummoner(summoners.get(0).getName(),summoners.get(0).getRegion(),summoners.get(0).getProfileIconId());
            } else if (selected == 0)
                startLogin();
        }
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
        databaseHandler.close();
    }

    public void setSummoner(String summonerName, String region, final int iconId) {
        Drawable iconDrawable;

        final Button testBtn = (Button) findViewById(R.id.summonersBtn);
        Icon icon = databaseHandler.getSummonerIcon(iconId);
        String btnText = "<font color='white'>" + summonerName + "</font> <font color='#153d3d'>" + region + "</font>";
        testBtn.setText(Html.fromHtml(btnText), Button.BufferType.SPANNABLE);
        if (icon == null) {
            RequestParams requestParams = new RequestParams();
            requestParams.put("region", region);
            requestParams.put("id", iconId);
            LeagueAssistantAPI.get("leagueAssistant/icons/summoner", requestParams, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    Icon saveIcon = new Icon(iconId, responseBody);
                    databaseHandler.addSummonerIcon(saveIcon);
                    Bitmap bitmap = BitmapFactory.decodeByteArray(saveIcon.getIcon(), 0, saveIcon.getIcon().length);
                    Drawable iconDrawable = new BitmapDrawable(getResources(), getCroppedBitmap(bitmap));
                    testBtn.setCompoundDrawablesWithIntrinsicBounds(iconDrawable, null, null, null);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] response, Throwable throwable) {
                    if (statusCode == 0) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                        builder.setMessage("Please check your internet connection and try again").setTitle("Error");
                        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                            }
                        });
                        AlertDialog dialog = builder.create();
                        dialog.show();
                    }
                }
            });
        } else {
            Bitmap bitmap = BitmapFactory.decodeByteArray(icon.getIcon(), 0, icon.getIcon().length);
            iconDrawable = new BitmapDrawable(getResources(), getCroppedBitmap(bitmap));
            testBtn.setCompoundDrawablesWithIntrinsicBounds(iconDrawable, null, null, null);
        }
    }

    public Bitmap getCroppedBitmap(Bitmap bitmap) {
        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),
                bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawCircle(bitmap.getWidth() / 2, bitmap.getHeight() / 2,
                bitmap.getWidth() / 2, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);
        Bitmap bmp = Bitmap.createScaledBitmap(output, 110, 110, false);
        return bmp;
    }

    private boolean isNetworkAvailable() {

        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}

