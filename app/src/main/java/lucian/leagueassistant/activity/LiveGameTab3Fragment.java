package lucian.leagueassistant.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import lucian.leagueassistant.R;
import lucian.leagueassistant.model.LiveGame;
import lucian.leagueassistant.model.LiveGameParticipant;
import lucian.leagueassistant.model.Summoner;
import lucian.leagueassistant.service.DatabaseHandler;
import lucian.leagueassistant.service.GameCache;

/**
 * Created by Lucian on 15-Jun-17.
 */

public class LiveGameTab3Fragment extends Fragment {

    private GameCache gameCache = GameCache.getInstance();
    private DatabaseHandler databaseHandler;
    private EnemyAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View V = inflater.inflate(R.layout.live_game_tab3, container, false);

        return V;
    }

    @Override
    public void onResume() {
        super.onResume();
        databaseHandler = new DatabaseHandler(getContext());

        ListView teamList = (ListView) getActivity().findViewById(R.id.enemyList);
        long summonerId = getActivity().getIntent().getLongExtra("summonerId", 0L);
        if (gameCache.getCache(summonerId) == null)
            getActivity().finish();
        else {
            LiveGame liveGame = gameCache.getCache(summonerId).getLiveGame();
            List<LiveGameParticipant> participants = liveGame.getParticipants();
            List<LiveGameParticipant> team = new ArrayList<>();
            int teamId = 0;
            for (LiveGameParticipant participant : participants) {
                if (participant.getSummonerId() == summonerId) {
                    teamId = participant.getTeamId();
                }
            }
            for (LiveGameParticipant participant : participants) {
                if (participant.getTeamId() != teamId) {
                    team.add(participant);
                }
            }
            Summoner summoner = databaseHandler.getSummoner(summonerId);
            adapter = new EnemyAdapter(this.getContext(), team, summoner);
            teamList.setAdapter(adapter);
            adapter.update();
        }
    }
}
