package lucian.leagueassistant.activity;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import lucian.leagueassistant.R;
import lucian.leagueassistant.model.Icon;
import lucian.leagueassistant.model.Summoner;
import lucian.leagueassistant.service.DatabaseHandler;

/**
 * Created by Lucian on 14-Jun-17.
 */

public class SummonerListAdapter extends ArrayAdapter<Summoner> {
    private final Context context;
    private List<Summoner> itemsArrayList;
    private DatabaseHandler databaseHandler;

    public SummonerListAdapter(Context context, DatabaseHandler databaseHandler) {
        super(context, R.layout.summoner_list_row, databaseHandler.getAllSummoners());
        this.context = context;
        this.itemsArrayList = databaseHandler.getAllSummoners();
        this.databaseHandler = databaseHandler;
    }

    public void update() {
        this.itemsArrayList.clear();
        this.itemsArrayList = databaseHandler.getAllSummoners();
        this.notifyDataSetChanged();

    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rowView = inflater.inflate(R.layout.summoner_list_row, parent, false);

        ImageView iconView = (ImageView) rowView.findViewById(R.id.summonerIcon);
        TextView summonerName = (TextView) rowView.findViewById(R.id.summonerName);
        TextView region = (TextView) rowView.findViewById(R.id.region);
        ImageButton checkBtn = (ImageButton) rowView.findViewById(R.id.checkBtn);

        Icon icon = databaseHandler.getSummonerIcon(itemsArrayList.get(position).getProfileIconId());
        if (icon != null) {
            Bitmap bitmap = BitmapFactory.decodeByteArray(icon.getIcon(), 0, icon.getIcon().length);
            Drawable iconDrawable = new BitmapDrawable(bitmap);
            iconView.setImageDrawable(iconDrawable);
        }
        else {
            iconView.setImageResource(R.drawable.default_summoner_icon);
        }

        summonerName.setText(itemsArrayList.get(position).getName());
        region.setText(itemsArrayList.get(position).getRegion());
        if (itemsArrayList.get(position).isSelected())
            checkBtn.setBackgroundResource(R.drawable.checked);
        else checkBtn.setBackgroundResource(R.drawable.check);
        return rowView;
    }

    @Override
    public int getCount() {
        return this.itemsArrayList.size();
    }

    @Override
    public Summoner getItem(int position) {
        return this.itemsArrayList.get(position);
    }


}
