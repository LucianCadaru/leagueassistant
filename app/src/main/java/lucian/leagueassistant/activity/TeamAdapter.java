package lucian.leagueassistant.activity;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.gson.Gson;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;
import java.util.regex.Pattern;

import cz.msebera.android.httpclient.Header;
import lucian.leagueassistant.R;
import lucian.leagueassistant.model.Champion;
import lucian.leagueassistant.model.LiveGameParticipant;
import lucian.leagueassistant.model.Mastery;
import lucian.leagueassistant.model.Summoner;
import lucian.leagueassistant.service.DatabaseHandler;
import lucian.leagueassistant.service.LeagueAssistantAPI;

/**
 * Created by Lucian on 17-Jun-17.
 */

public class TeamAdapter extends ArrayAdapter<LiveGameParticipant> {

    private final Context context;
    private List<LiveGameParticipant> participants;
    private DatabaseHandler databaseHandler;
    private Summoner summoner;

    public TeamAdapter(Context context, List<LiveGameParticipant> participants, Summoner callingSummoner) {
        super(context, R.layout.champ_list_item, participants);
        this.context = context;
        this.participants = participants;
        this.databaseHandler = new DatabaseHandler(getContext());
        this.summoner = callingSummoner;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rowView = inflater.inflate(R.layout.champ_list_item, parent, false);

        TextView summonerNameView = (TextView) rowView.findViewById(R.id.summonerListName);
        final TextView rank = (TextView) rowView.findViewById(R.id.rank);
        summonerNameView.setText(participants.get(position).getSummonerName());

        final ImageView championIcon = (ImageView) rowView.findViewById(R.id.champIcon);
        Champion champion = databaseHandler.getChampion(participants.get(position).getChampionId());
        if (champion == null) {
            RequestParams requestParams = new RequestParams();
            requestParams.put("region", summoner.getRegion());
            requestParams.put("id", participants.get(position).getChampionId());
            LeagueAssistantAPI.get("leagueAssistant/champion", requestParams, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject responseBody) {
                    Gson gson = new Gson();
                    final Champion champion = gson.fromJson(responseBody.toString(), Champion.class);
                    getChampionIcon(championIcon, champion.getKey());
                    databaseHandler.addChampion(champion);
                }


            });
        } else {
            getChampionIcon(championIcon, champion.getKey());
        }
        setRank(rank, participants.get(position).getSummonerId());


        ImageView spell1 = (ImageView) rowView.findViewById(R.id.summonerSpell1);
        ImageView spell2 = (ImageView) rowView.findViewById(R.id.summonerSpell2);
        long spellId1 = participants.get(position).getSpell1Id();
        long spellId2 = participants.get(position).getSpell2Id();
        setSummonersSpell(spell1,spellId1);
        setSummonersSpell(spell2,spellId2);
        ImageView keystoneImg = (ImageView) rowView.findViewById(R.id.keystone);
        setKeystoneMastery(participants.get(position).getMasteries(),keystoneImg);


        return rowView;
    }

    private void setRank(final TextView rank, long summonerId){
        RequestParams requestParams = new RequestParams();
        requestParams.put("region", summoner.getRegion());
        requestParams.put("id", summonerId);
        LeagueAssistantAPI.get("leagueAssistant/rank", requestParams, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject responseBody) {
                try {
                    if (statusCode == 204) {
                        rank.setText("Unranked");
                    } else {
                        String tier = responseBody.get("tier").toString();
                        tier = tier.substring(0, 1).toUpperCase() + tier.substring(1, tier.length()).toLowerCase();
                        rank.setText(tier + " " + responseBody.get("rank").toString());
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void setKeystoneMastery(List<Mastery> masteries,ImageView imageView){
        Pattern p = Pattern.compile("^6[1-3]6[1-3]$");
        for (Mastery mastery:masteries){
            if(p.matcher(String.valueOf(mastery.getMasteryId())).matches())
            {
                Glide.with(getContext())
                        .load("http://ddragon.leagueoflegends.com/cdn/7.12.1/img/mastery/" + mastery.getMasteryId() + ".png")
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(imageView);
            }

        }
    }

    private void setSummonersSpell(ImageView imageView, long spellId) {
        if (spellId == 1)
            imageView.setImageResource(R.drawable.summoner_cleanse);
        if (spellId == 3)
            imageView.setImageResource(R.drawable.summoner_exhaust);
        if (spellId == 4)
            imageView.setImageResource(R.drawable.summoner_flash);
        if (spellId == 6)
            imageView.setImageResource(R.drawable.summoner_haste);
        if (spellId == 7)
            imageView.setImageResource(R.drawable.summoner_heal);
        if (spellId == 11)
            imageView.setImageResource(R.drawable.summoner_smite);
        if (spellId == 12)
            imageView.setImageResource(R.drawable.summoner_teleport);
        if (spellId == 13)
            imageView.setImageResource(R.drawable.summoner_mana);
        if (spellId == 14)
            imageView.setImageResource(R.drawable.summoner_ignite);
        if (spellId == 21)
            imageView.setImageResource(R.drawable.summoner_barrier);
        if (spellId == 32)
            imageView.setImageResource(R.drawable.summoner_snowball);
    }

    public void getChampionIcon(ImageView imageView, String key) {
        //TODO: GET LATEST VERSION
        Glide.with(getContext())
                .load("http://ddragon.leagueoflegends.com/cdn/7.12.1/img/champion/" + key + ".png")
                .transform(new CircularTransformation(getContext()))
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(imageView);
    }

    @Override
    public int getCount() {
        return this.participants.size();
    }

    @Override
    public LiveGameParticipant getItem(int position) {
        return this.participants.get(position);
    }

    public void update() {
        this.notifyDataSetChanged();
    }

}

