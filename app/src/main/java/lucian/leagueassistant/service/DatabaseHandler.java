package lucian.leagueassistant.service;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lucian.leagueassistant.model.Champion;
import lucian.leagueassistant.model.Icon;
import lucian.leagueassistant.model.ItemDto;
import lucian.leagueassistant.model.MasteryDto;
import lucian.leagueassistant.model.RuneDto;
import lucian.leagueassistant.model.Spell;
import lucian.leagueassistant.model.Summoner;

/**
 * Created by Lucian on 14-Jun-17.
 */
public class DatabaseHandler extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "leagueDatabase";
    private static final String TABLE_SUMMONERS = "summoners";
    private static final String TABLE_CHAMPIONS = "champions";
    private static final String TABLE_SPELLS = "spells";
    private static final String TABLE_RUNES = "runes";
    private static final String TABLE_ITEMS = "items";
    private static final String TABLE_MASTERIES = "masteries";
    private static final String TABLE_MASTERIES_DESCRIPTION = "masteriesDescription";
    private static final String TABLE_SUMMONER_ICONS = "summonerIcons";


    private static final String KEY_ID = "id";
    private static final String KEY_SLOT_ID = "slotId";
    private static final String KEY_TYPE = "type";
    private static final String KEY_GOLD = "gold";
    private static final String KEY_CHAMPION_ID = "championId";
    private static final String KEY_NAME = "name";
    private static final String KEY_KEY = "key";
    private static final String KEY_SPELL_SLOT = "spellSlot";
    private static final String KEY_REGION = "region";
    private static final String KEY_DESCRIPTION = "description";
    private static final String KEY_ACCOUNT_ID = "accountId";
    private static final String KEY_PROFILE_ICON_ID = "profileIconId";
    private static final String KEY_SELECTED = "selected";
    private static final String KEY_ICON = "icon";


    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_SUMMONERS_TABLE = "CREATE TABLE " + TABLE_SUMMONERS + "("
                + KEY_ID + " INTEGER PRIMARY KEY," + KEY_NAME + " TEXT,"
                + KEY_REGION + " TEXT," + KEY_ACCOUNT_ID + " INTEGER," + KEY_PROFILE_ICON_ID + " INTEGER," + KEY_SELECTED + " INTEGER" + ")";

        String CREATE_CHAMPIONS_TABLE = "CREATE TABLE " + TABLE_CHAMPIONS + "("
                + KEY_ID + " INTEGER PRIMARY KEY," + KEY_NAME + " TEXT,"
                + KEY_KEY + " TEXT" + ")";

        String CREATE_SPELLS_TABLE = "CREATE TABLE " + TABLE_SPELLS + "("
                + KEY_KEY + " TEXT PRIMARY KEY," + KEY_NAME + " TEXT,"
                + KEY_DESCRIPTION + " TEXT," + KEY_SPELL_SLOT + " INTEGER," + KEY_CHAMPION_ID + " INTEGER, FOREIGN KEY (" + KEY_CHAMPION_ID + ") REFERENCES " + TABLE_CHAMPIONS + "(" + KEY_ID + "))";

        String CREATE_SUMMONER_ICONS_TABLE = "CREATE TABLE " + TABLE_SUMMONER_ICONS + "("
                + KEY_ID + " INTEGER PRIMARY KEY," + KEY_ICON + " BLOB" + ")";

        String CREATE_ITEMS_TABLE = "CREATE TABLE " + TABLE_ITEMS + "("
                + KEY_ID + " INTEGER PRIMARY KEY," + KEY_NAME + " TEXT,"
                + KEY_DESCRIPTION + " TEXT," + KEY_GOLD + " INTEGER" + ")";

        String CREATE_RUNES_TABLE = "CREATE TABLE " + TABLE_RUNES + "("
                + KEY_ID + " INTEGER PRIMARY KEY," + KEY_NAME + " TEXT,"
                + KEY_TYPE + " TEXT," + KEY_KEY + " TEXT," + KEY_DESCRIPTION + " TEXT" + ")";

        String CREATE_MASTERIES_TABLE = "CREATE TABLE " + TABLE_MASTERIES + "("
                + KEY_ID + " INTEGER PRIMARY KEY," + KEY_NAME + " TEXT" + ")";

        String CREATE_MASTERIES_DESCRIPTION_TABLE = "CREATE TABLE " + TABLE_MASTERIES_DESCRIPTION + "("
                + KEY_ID + " INTEGER," + KEY_DESCRIPTION + " TEXT," + KEY_SLOT_ID + " INTEGER, FOREIGN KEY (" + KEY_ID + ") REFERENCES " + TABLE_MASTERIES + "(" + KEY_ID + "))";


        db.execSQL(CREATE_SUMMONERS_TABLE);
        db.execSQL(CREATE_CHAMPIONS_TABLE);
        db.execSQL(CREATE_SPELLS_TABLE);
        db.execSQL(CREATE_SUMMONER_ICONS_TABLE);
        db.execSQL(CREATE_ITEMS_TABLE);
        db.execSQL(CREATE_MASTERIES_TABLE);
        db.execSQL(CREATE_MASTERIES_DESCRIPTION_TABLE);
        db.execSQL(CREATE_RUNES_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SUMMONERS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CHAMPIONS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SPELLS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SUMMONER_ICONS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_RUNES);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_MASTERIES_DESCRIPTION);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_MASTERIES);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ITEMS);
        onCreate(db);
    }


    public void addSummonerIcon(Icon summonerIcon) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_ID, summonerIcon.getId());
        values.put(KEY_ICON, summonerIcon.getIcon());

        db.insert(TABLE_SUMMONER_ICONS, null, values);


    }

    public Icon getSummonerIcon(int summonerId) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_SUMMONER_ICONS, new String[]{KEY_ID,
                        KEY_ICON}, KEY_ID + "=?",
                new String[]{String.valueOf(summonerId)}, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();

        if (cursor.getCount() > 0) {
            Icon summonerIcon = new Icon(cursor.getInt(0), cursor.getBlob(1));

            return summonerIcon;
        } else {

            return null;
        }

    }

    public void addSummoner(Summoner summoner) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_ID, summoner.getId());
        values.put(KEY_NAME, summoner.getName());
        values.put(KEY_REGION, summoner.getRegion());
        values.put(KEY_ACCOUNT_ID, summoner.getAccountId());
        values.put(KEY_PROFILE_ICON_ID, summoner.getProfileIconId());
        values.put(KEY_SELECTED, (summoner.isSelected() ? 1 : 0));

        db.insert(TABLE_SUMMONERS, null, values);

    }

    public Summoner getSummoner(long id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_SUMMONERS, new String[]{KEY_ID,
                        KEY_NAME, KEY_REGION, KEY_ACCOUNT_ID, KEY_PROFILE_ICON_ID, KEY_SELECTED}, KEY_ID + "=?",
                new String[]{String.valueOf(id)}, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();

        Summoner summoner = new Summoner(Integer.parseInt(cursor.getString(0)),
                cursor.getString(1), cursor.getString(2), Long.parseLong(cursor.getString(3)), cursor.getInt(4), cursor.getInt(5));

        return summoner;
    }

    public List<Summoner> getAllSummoners() {
        List<Summoner> summonerList = new ArrayList<>();
        String selectQuery = "SELECT  * FROM " + TABLE_SUMMONERS;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                Summoner summoner = new Summoner();
                summoner.setId(Integer.parseInt(cursor.getString(0)));
                summoner.setName(cursor.getString(1));
                summoner.setRegion(cursor.getString(2));
                summoner.setAccountId(Long.parseLong(cursor.getString(3)));
                summoner.setProfileIconId(cursor.getInt(4));
                if (cursor.getInt(5) == 1)
                    summoner.setSelected(true);
                else summoner.setSelected(false);

                summonerList.add(summoner);
            } while (cursor.moveToNext());
        }

        return summonerList;
    }

    public void updateSummoner(Summoner summoner) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_ID, summoner.getId());
        values.put(KEY_NAME, summoner.getName());
        values.put(KEY_REGION, summoner.getRegion());
        values.put(KEY_ACCOUNT_ID, summoner.getAccountId());
        values.put(KEY_PROFILE_ICON_ID, summoner.getProfileIconId());
        values.put(KEY_SELECTED, summoner.isSelected());


        db.update(TABLE_SUMMONERS, values, KEY_ID + " = ?",
                new String[]{String.valueOf(summoner.getId())});

    }

    public void removeSummoner(Summoner summoner) {
        SQLiteDatabase db = this.getWritableDatabase();

        db.delete(TABLE_SUMMONERS, KEY_ID + " = ?", new String[]{String.valueOf(summoner.getId())});

    }

    public Summoner getSummoner(String name, String region) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_SUMMONERS, new String[]{KEY_ID,
                        KEY_NAME, KEY_REGION, KEY_ACCOUNT_ID, KEY_PROFILE_ICON_ID, KEY_SELECTED},
                KEY_NAME + "=? AND " + KEY_REGION + "=?",
                new String[]{name, region}, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();

        Summoner summoner = new Summoner(cursor.getInt(0), cursor.getString(1), cursor.getString(2),
                Long.parseLong(cursor.getString(3)), cursor.getInt(4), cursor.getInt(5));
        cursor.close();
        return summoner;
    }

    public void addChampion(Champion champion) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_ID, champion.getId());
        values.put(KEY_NAME, champion.getName());
        values.put(KEY_KEY, champion.getKey());
        db.insert(TABLE_CHAMPIONS, null, values);
        for (Spell spell : champion.getSpells()) {
            addSpell(spell, champion.getId());
        }

    }

    public Champion getChampion(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_CHAMPIONS, new String[]{KEY_ID,
                        KEY_NAME, KEY_KEY}, KEY_ID + "=?",
                new String[]{String.valueOf(id)}, null, null, null, null);
        if (cursor != null && cursor.moveToFirst()) {
            Champion champion = new Champion(cursor.getInt(0), cursor.getString(1), cursor.getString(2));
            List<Spell> spells = getSpellsOfChampion(id);
            champion.setSpells(spells);

            return champion;
        }

        return null;

    }

    public void addSpell(Spell spell, int championId) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_KEY, spell.getKey());
        values.put(KEY_NAME, spell.getName());
        values.put(KEY_DESCRIPTION, spell.getDescription());
        values.put(KEY_CHAMPION_ID, championId);
        db.insert(TABLE_SPELLS, null, values);

    }

    public List<Spell> getSpellsOfChampion(int championId) {
        List<Spell> spells = new ArrayList<>();
        String selectQuery = "SELECT  * FROM " + TABLE_SPELLS + " WHERE " + KEY_CHAMPION_ID + "=" + championId;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                Spell spell = new Spell(cursor.getString(0), cursor.getString(1), cursor.getString(2), cursor.getInt(3));
                spells.add(spell);
            } while (cursor.moveToNext());
        }
        return spells;
    }

    public void addRune(RuneDto rune) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_ID, rune.getId());
        values.put(KEY_NAME, rune.getName());
        values.put(KEY_KEY, rune.getKey());
        values.put(KEY_DESCRIPTION, rune.getDescription());
        values.put(KEY_TYPE, rune.getType());
        db.insert(TABLE_RUNES, null, values);

    }

    public RuneDto getRune(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_RUNES, new String[]{KEY_ID,
                        KEY_NAME, KEY_KEY, KEY_DESCRIPTION, KEY_TYPE}, KEY_ID + "=?",
                new String[]{String.valueOf(id)}, null, null, null, null);
        if (cursor != null && cursor.moveToFirst()) {
            RuneDto runeDto = new RuneDto();
            runeDto.setId(cursor.getInt(0));
            runeDto.setName(cursor.getString(1));
            runeDto.setKey(cursor.getString(2));
            runeDto.setDescription(cursor.getString(3));
            runeDto.setType(cursor.getString(4));

            return runeDto;
        }

        return null;
    }

    public void addMastery(MasteryDto masteryDto) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_ID, masteryDto.getId());
        values.put(KEY_NAME, masteryDto.getName());
        db.insert(TABLE_MASTERIES, null, values);
        addMasteryDescription(masteryDto.getDescription(), masteryDto.getId());

    }

    public MasteryDto getMastery(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_MASTERIES, new String[]{KEY_ID,
                        KEY_NAME}, KEY_ID + "=?",
                new String[]{String.valueOf(id)}, null, null, null, null);
        if (cursor != null && cursor.moveToFirst()) {
            MasteryDto masteryDto = new MasteryDto();
            masteryDto.setId(cursor.getInt(0));
            masteryDto.setName(cursor.getString(1));
            masteryDto.setDescription(getMasteryDescription(masteryDto.getId()));

            return masteryDto;
        }

        return null;
    }

    public void addMasteryDescription(String[] descriptions, int masteryId) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values;
        for (int i = 0; i < descriptions.length; i++) {
            values = new ContentValues();
            values.put(KEY_ID, masteryId);
            values.put(KEY_DESCRIPTION, descriptions[i]);
            values.put(KEY_SLOT_ID, i);
            db.insert(TABLE_MASTERIES_DESCRIPTION, null, values);
        }

    }

    public String[] getMasteryDescription(int masteryId) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_MASTERIES_DESCRIPTION, new String[]{KEY_ID,
                        KEY_DESCRIPTION, KEY_SLOT_ID}, KEY_ID + "=?",
                new String[]{String.valueOf(masteryId)}, null, null, null, null);
        String[] descriptions;
        Map<Integer, String> descriptionsMap = new HashMap<>();
        if (cursor.moveToFirst()) {
            do {
                descriptionsMap.put(cursor.getInt(2), cursor.getString(1));
            } while (cursor.moveToNext());
        }
        descriptions = new String[descriptionsMap.size()];
        for (int i = 0; i < descriptionsMap.size(); i++) {
            descriptions[i] = descriptionsMap.get(i);
        }
        return descriptions;
    }

    public void addItem(ItemDto itemDto) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_ID, itemDto.getId());
        values.put(KEY_NAME, itemDto.getName());
        values.put(KEY_DESCRIPTION, itemDto.getDescription());
        values.put(KEY_GOLD, itemDto.getGold());
        db.insert(TABLE_ITEMS, null, values);

    }

    public ItemDto getItem(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_ITEMS, new String[]{KEY_ID,
                        KEY_NAME, KEY_DESCRIPTION, KEY_GOLD}, KEY_ID + "=?",
                new String[]{String.valueOf(id)}, null, null, null, null);
        if (cursor != null && cursor.moveToFirst()) {
            ItemDto itemDto = new ItemDto();
            itemDto.setId(cursor.getInt(0));
            itemDto.setName(cursor.getString(1));
            itemDto.setDescription(cursor.getString(2));
            itemDto.setGold(cursor.getInt(3));

            return itemDto;
        }

        return null;
    }

    public boolean isRunesEmpty() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("select * from " + TABLE_RUNES, null);
        if (cursor.getCount() <= 0)
            return true;

        return false;
    }

    public boolean isItemsEmpty() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("select * from " + TABLE_ITEMS, null);
        if (cursor.getCount() <= 0)
            return true;

        return false;
    }

    public boolean isMasteriesEmpty() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("select * from " + TABLE_MASTERIES, null);
        if (cursor.getCount() <= 0)
            return true;

        return false;
    }


}
