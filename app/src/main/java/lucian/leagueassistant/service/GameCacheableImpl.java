package lucian.leagueassistant.service;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import lucian.leagueassistant.model.LiveGame;
import lucian.leagueassistant.model.Summoner;

/**
 * Created by Lucian on 15-Jun-17.
 */

public class GameCacheableImpl implements GameCacheable {

    private Date dateOfExpiration;
    private long summonerId;
    private LiveGame liveGame;

    public GameCacheableImpl(LiveGame liveGame, Long id, int minutesToLive) {
        this.liveGame = liveGame;
        this.summonerId = id;
        if (minutesToLive != 0) {
            dateOfExpiration = new java.util.Date();
            java.util.Calendar calendar = java.util.Calendar.getInstance();
            calendar.setTime(dateOfExpiration);
            calendar.add(calendar.MINUTE, minutesToLive);
            dateOfExpiration = calendar.getTime();
        }
    }

    @Override
    public boolean isExpired() {
        if (dateOfExpiration != null) {
            if (dateOfExpiration.before(new Date()))
                return true;
            else return false;
        } else
            return false;
    }

    @Override
    public long getIdentifier() {
        return summonerId;
    }

    public LiveGame getLiveGame(){
        return liveGame;
    }
}
