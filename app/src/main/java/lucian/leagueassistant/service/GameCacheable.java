package lucian.leagueassistant.service;

import lucian.leagueassistant.model.LiveGame;

/**
 * Created by Lucian on 16-Jun-17.
 */

public interface GameCacheable {

    boolean isExpired();
    long getIdentifier();
    LiveGame getLiveGame();
}
