package lucian.leagueassistant.service;

import android.content.Intent;
import android.util.Log;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * Created by Lucian on 16-Jun-17.
 */
public class GameCache {
    private static Map<Long, GameCacheable> cacheHashMap = new HashMap<Long, GameCacheable>();
    private static GameCache gameCacheInstance;

    private GameCache() {
    }

    static {
        try {
            Thread threadCleanerUpper = new Thread(
                    new Runnable() {
                        int milliSecondSleepTime = 5000;

                        public void run() {
                            try {
                                while (true) {
                                    Set keySet = cacheHashMap.keySet();
                                    Iterator keys = keySet.iterator();
                                    while (keys.hasNext()) {
                                        Object key = keys.next();
                                        GameCacheable value = cacheHashMap.get(key);
                                        if (value.isExpired()) {
                                            cacheHashMap.remove(key);
                                        }
                                    }
                                    Thread.sleep(this.milliSecondSleepTime);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });
            threadCleanerUpper.setPriority(Thread.MIN_PRIORITY);
            threadCleanerUpper.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void putCache(GameCacheable cachedGame) {
        cacheHashMap.put(cachedGame.getIdentifier(), cachedGame);
    }

    public static GameCacheable getCache(long identifier) {
        GameCacheable cachedGame = cacheHashMap.get(identifier);
        Set keySet = cacheHashMap.keySet();
        Iterator keys = keySet.iterator();

        if (cachedGame == null)
            return null;
        if (cachedGame.isExpired()) {
            cacheHashMap.remove(identifier);
            return null;
        } else {
            return cachedGame;
        }
    }

    public static GameCache getInstance() {
        if (gameCacheInstance == null) {
            gameCacheInstance = new GameCache();
            return gameCacheInstance;
        } else
            return gameCacheInstance;
    }
}