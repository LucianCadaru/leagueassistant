package lucian.leagueassistant.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Lucian on 14-Jun-17.
 */

public class LiveGameParticipant {

    @SerializedName("profileIconId")
    @Expose
    private int profileIconId;
    @SerializedName("championId")
    @Expose
    private int championId;
    @SerializedName("summonerName")
    @Expose
    private String summonerName;
    @SerializedName("runes")
    @Expose
    private List<Rune> runes = null;
    @SerializedName("bot")
    @Expose
    private Boolean bot;
    @SerializedName("masteries")
    @Expose
    private List<Mastery> masteries = null;
    @SerializedName("spell2Id")
    @Expose
    private long spell2Id;
    @SerializedName("teamId")
    @Expose
    private int teamId;
    @SerializedName("spell1Id")
    @Expose
    private long spell1Id;
    @SerializedName("summonerId")
    @Expose
    private long summonerId;

    public int getProfileIconId() {
        return profileIconId;
    }

    public void setProfileIconId(int profileIconId) {
        this.profileIconId = profileIconId;
    }

    public int getChampionId() {
        return championId;
    }

    public void setChampionId(int championId) {
        this.championId = championId;
    }

    public String getSummonerName() {
        return summonerName;
    }

    public void setSummonerName(String summonerName) {
        this.summonerName = summonerName;
    }

    public List<Rune> getRunes() {
        return runes;
    }

    public void setRunes(List<Rune> runes) {
        this.runes = runes;
    }

    public Boolean getBot() {
        return bot;
    }

    public void setBot(Boolean bot) {
        this.bot = bot;
    }

    public List<Mastery> getMasteries() {
        return masteries;
    }

    public void setMasteries(List<Mastery> masteries) {
        this.masteries = masteries;
    }

    public long getSpell2Id() {
        return spell2Id;
    }

    public void setSpell2Id(long spell2Id) {
        this.spell2Id = spell2Id;
    }

    public int getTeamId() {
        return teamId;
    }

    public void setTeamId(int teamId) {
        this.teamId = teamId;
    }

    public long getSpell1Id() {
        return spell1Id;
    }

    public void setSpell1Id(long spell1Id) {
        this.spell1Id = spell1Id;
    }

    public long getSummonerId() {
        return summonerId;
    }

    public void setSummonerId(long summonerId) {
        this.summonerId = summonerId;
    }

}
