package lucian.leagueassistant.model;

/**
 * Created by Lucian on 14-Jun-17.
 */

public class Icon {
    private int id;
    private byte[] icon;

    public Icon(int id,byte[] icon){
        setId(id);
        setIcon(icon);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public byte[] getIcon() {
        return icon;
    }

    public void setIcon(byte[] icon) {
        this.icon = icon;
    }
}
