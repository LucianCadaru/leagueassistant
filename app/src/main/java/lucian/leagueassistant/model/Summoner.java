package lucian.leagueassistant.model;

/**
 * Created by Lucian on 14-Jun-17.
 */

public class Summoner {
    private long id;
    private String name;
    private String region;
    private int profileIconId;
    private long accountId;
    private boolean selected;

    public Summoner() {
    }

    public Summoner(long id, String name, String region, long accountId, int profileIconId,int selected) {
        setId(id);
        setName(name);
        setRegion(region);
        setAccountId(accountId);
        setProfileIconId(profileIconId);
        if (selected==1)
            setSelected(true);
        else setSelected(false);

    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public int getProfileIconId() {
        return profileIconId;
    }

    public void setProfileIconId(int profileIconId) {
        this.profileIconId = profileIconId;
    }

    public long getAccountId() {
        return accountId;
    }

    public void setAccountId(long accountId) {
        this.accountId = accountId;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}
