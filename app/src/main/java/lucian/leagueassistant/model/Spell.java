package lucian.leagueassistant.model;

public class Spell {

    private String key;
    private String description;
    private String name;
    private int spellSlot;

    public Spell(String key, String name, String description,int spellSlot) {
        setName(name);
        setKey(key);
        setDescription(description);
        setSpellSlot(spellSlot);
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSpellSlot() {
        return spellSlot;
    }

    public void setSpellSlot(int spellSlot) {
        this.spellSlot = spellSlot;
    }
}
