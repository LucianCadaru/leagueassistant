package lucian.leagueassistant.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Lucian on 14-Jun-17.
 */

public class BannedChampion {

    @SerializedName("teamId")
    @Expose
    private Integer teamId;
    @SerializedName("championId")
    @Expose
    private long championId;
    @SerializedName("pickTurn")
    @Expose
    private long pickTurn;

    public Integer getTeamId() {
        return teamId;
    }

    public void setTeamId(Integer teamId) {
        this.teamId = teamId;
    }

    public long getChampionId() {
        return championId;
    }

    public void setChampionId(long championId) {
        this.championId = championId;
    }

    public long getPickTurn() {
        return pickTurn;
    }

    public void setPickTurn(long pickTurn) {
        this.pickTurn = pickTurn;
    }

}
