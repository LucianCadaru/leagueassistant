package lucian.leagueassistant.model;

/**
 * Created by Lucian on 14-Jun-17.
 */

public class NamedIcon {
    private String name;
    private byte[] icon;

    public NamedIcon(String name, byte[] icon) {
        setName(name);
        setIcon(icon);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public byte[] getIcon() {
        return icon;
    }

    public void setIcon(byte[] icon) {
        this.icon = icon;
    }
}
