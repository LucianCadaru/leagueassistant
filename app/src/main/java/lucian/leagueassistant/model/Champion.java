package lucian.leagueassistant.model;

import java.util.List;


public class Champion {

    private String name;
    private String key;
    private Integer id;
    private List<Spell> spells = null;

    public Champion(Integer id,String name,String key){
        setId(id);
        setKey(key);
        setName(name);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<Spell> getSpells() {
        return spells;
    }

    public void setSpells(List<Spell> spells) {
        this.spells = spells;
    }

}
